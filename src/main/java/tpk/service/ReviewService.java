package tpk.service;

import org.springframework.beans.factory.annotation.Autowired;
import tpk.entity.Review;

import java.util.List;

public class ReviewService {
    public double calculateAverageReviewStars(List<Review> reviews) {
        if (reviews.size() == 0)
            return 0;
        int allStarsSum = 0;
        for (Review r : reviews) {
            allStarsSum += r.getStars();
        }
        return allStarsSum / reviews.size();
    }
}
