package tpk.service;

import tpk.entity.Session;

public interface SessionService {
    Session findCurrentSession();

    void deleteCurrentSession();
}
