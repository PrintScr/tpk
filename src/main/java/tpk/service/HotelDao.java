package tpk.service;

import tpk.entity.Hotel;

import java.util.List;

public interface HotelDao {
    List<Hotel> searchAllHotels();
}
