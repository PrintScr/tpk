package tpk.service;

import tpk.entity.User;

import java.util.List;

public class LoginService {
    private LoginDao loginDao;
    private SessionService sessionService;

    public boolean login(String email, String password) {
        if (email.isEmpty() || password.isEmpty()) {
            return false;
        }
        List<User> allUsers = loginDao.getAllUsers();
        for (User user : allUsers) {
            if (user.getEmail().equals(email)
                    && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public boolean logout() {
        if (sessionService.findCurrentSession() != null) {
            sessionService.deleteCurrentSession();
            return true;
        }
        return false;
    }
}