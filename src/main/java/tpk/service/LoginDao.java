package tpk.service;

import tpk.entity.User;

import java.util.List;

public interface LoginDao {
    List<User> getAllUsers();
}
