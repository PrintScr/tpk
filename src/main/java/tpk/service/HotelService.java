package tpk.service;

import tpk.entity.AccommodationFilter;
import tpk.entity.Hotel;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class HotelService {
    private HotelDao hotelDao;

    public List<Hotel> searchHotels(AccommodationFilter filter) {
        List<Hotel> allHotels = hotelDao.searchAllHotels();

        List<Hotel> filteredHotels = new ArrayList<>();

        for (Hotel hotel : allHotels) {
            if (isFiltersEmpty(filter)) {
                filteredHotels.add(hotel);
                continue;
            }
            boolean suits = false;
            if(isSuits(hotel::getHotelBrand, filter::getHotelBrands)
                    && isSuits(hotel::getLocation, filter::getLocations)
                    && isSuits(hotel::getStyle, filter::getStyles)
                    && isSuits(hotel::getStars, filter::getStars)){
                suits = true;
            }
            if (suits)
                filteredHotels.add(hotel);
        }

        return filteredHotels;
    }

    private <T> boolean isSuits(Supplier<T> hotelExtractor, Supplier<List<T>> hotelBrandsExtractor) {
        if(hotelBrandsExtractor.get().isEmpty()){
            return true;
        }
        for (T hotelBrandFilter : hotelBrandsExtractor.get()) {
            boolean suits = hotelExtractor.get().equals(hotelBrandFilter);
            if (suits)
                return true;
        }
        return false;
    }

    private boolean isFiltersEmpty(AccommodationFilter filter) {
        return filter.getHotelBrands().isEmpty()
                && filter.getLocations().isEmpty()
                && filter.getStars().isEmpty()
                && filter.getStyles().isEmpty();
    }

}