package tpk.entity;

import java.util.ArrayList;
import java.util.List;

public class AccommodationFilter {
    private List<String> hotelBrands = new ArrayList<>();
    private List<String> locations = new ArrayList<>();
    private List<Integer> stars = new ArrayList<>();
    private List<String> styles = new ArrayList<>();

    public AccommodationFilter() {
    }

    public AccommodationFilter(List<Integer> stars) {
        this.stars = stars;
    }

    public List<String> getHotelBrands() {
        return hotelBrands;
    }

    public void setHotelBrands(List<String> hotelBrands) {
        this.hotelBrands = hotelBrands;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public List<Integer> getStars() {
        return stars;
    }

    public void setStars(List<Integer> stars) {
        this.stars = stars;
    }

    public List<String> getStyles() {
        return styles;
    }

    public void setStyles(List<String> styles) {
        this.styles = styles;
    }
}
