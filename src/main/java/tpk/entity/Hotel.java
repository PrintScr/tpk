package tpk.entity;

public class Hotel {
    private String hotelBrand;
    private String location;
    private Integer stars;
    private String style;

    public Hotel() {
    }

    public Hotel(Integer stars) {
        this.stars = stars;
    }

    public Hotel(String hotelBrand, String location, Integer stars, String style) {
        this.hotelBrand = hotelBrand;
        this.location = location;
        this.stars = stars;
        this.style = style;
    }

    public String getHotelBrand() {
        return hotelBrand;
    }

    public void setHotelBrand(String hotelBrand) {
        this.hotelBrand = hotelBrand;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
