package tpk;

import com.google.common.collect.Lists;
import org.junit.Test;
import tpk.entity.Review;
import tpk.service.ReviewService;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ReviewServiceTest {
    private ReviewService service = new ReviewService();
    private static final double DELTA = 1e-10;

    @Test
    public void shouldReturnZero_noReviews() {
        double averageReviewStars = service.calculateAverageReviewStars(new ArrayList<>());
        assertEquals(0, averageReviewStars, DELTA);
    }

    @Test
    public void shouldReturnThree_oneFiveStarAndOneStarReview() {
        double averageReviewStars = service.calculateAverageReviewStars(
                Lists.newArrayList(new Review(5),new Review(1)));
        assertEquals(3,averageReviewStars,DELTA);
    }
}
