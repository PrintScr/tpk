package tpk;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import tpk.entity.Session;
import tpk.entity.User;
import tpk.service.LoginDao;
import tpk.service.LoginService;
import tpk.service.SessionService;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceTest {
    @InjectMocks
    LoginService loginService;
    @Mock
    LoginDao loginDao;
    @Mock
    SessionService sessionService;

    @Test
    public void logout_currentSessionExists(){
        when(sessionService.findCurrentSession()).thenReturn(new Session());

        boolean logoutSuccessful = loginService.logout();

        verify(sessionService).deleteCurrentSession();
        assertTrue(logoutSuccessful);
    }

    @Test
    public void login_matchingUserExists(){
        String email = "example@example.com";
        String password = "ex";
        when(loginDao.getAllUsers()).thenReturn(
                Lists.newArrayList(
                        new User(email,password),
                        new User("dfsgd@fsd.com","fds")));

        boolean login = loginService.login(email,password);

        assertTrue(login);
    }

    @Test
    public void login_noUsersFound(){
        boolean login = loginService.login("asfas@fsd.com","fdfds");

        assertFalse(login);
    }

    @Test
    public void login_noMatchingUserExists(){
        when(loginDao.getAllUsers()).thenReturn(
                Lists.newArrayList(
                        new User("da@ds.com","dsa"),
                        new User("dfsgd@fsd.com","fds")));

        boolean login = loginService.login("1@1.ru","12");

        assertFalse(login);
    }

}
