package tpk;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import tpk.entity.Hotel;
import tpk.entity.AccommodationFilter;
import tpk.service.HotelDao;
import tpk.service.HotelService;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HotelServiceTest {
    @InjectMocks
    HotelService hotelService;
    @Mock
    HotelDao hotelDao;

    @Test
    public void shouldFindAllHotels_emptyFilter() {
        Hotel hotel = new Hotel();
        when(hotelDao.searchAllHotels()).thenReturn(
                Lists.newArrayList(
                        hotel));

        List<Hotel> foundHotels = hotelService.searchHotels(new AccommodationFilter());

        assertThat(foundHotels).containsExactly(hotel);
    }

    @Test
    public void shouldFindFiveStarHotel_fiveStarFilter() {
        Hotel hoteFiveStar = new Hotel(5);
        Hotel hotelOneStar = new Hotel(1);

        when(hotelDao.searchAllHotels()).thenReturn(
                Lists.newArrayList(hoteFiveStar,hotelOneStar));

        Integer fiveStarFilter = 5;

        List<Hotel> foundHotels = hotelService.searchHotels(
                new AccommodationFilter(Lists.newArrayList(fiveStarFilter)));

        assertThat(foundHotels).containsExactly(hoteFiveStar);
    }
}